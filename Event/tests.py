from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import list, regist
from .forms import EventForm
from django.contrib.auth.models import User
from .models import EventModel, Event
# Create your tests here.

class EventUnitTest(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		
	def test_using_template(self):
		response = Client().get('/event/')
		self.assertTemplateUsed(response, 'Event/list.html')

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = EventForm()
		self.assertIn('class="form-control"', form.as_p())
		
	def test_google_member_regist_event_views(self):
		event = Event(eventsite='test-site', eventname='Event Name', eventimage='', eventdetail='Details', eventdate='2000-01-01')
		event.save()
		response = self.client.get('/register/test-site/')
		self.assertEqual(response.status_code, 302)
		user = User.objects.create_user(username='testuser', password='12345')
		login = self.client.login(username='testuser', password='12345')
		response = self.client.get('/register/test-site/')
		response = self.client.post('/register/test-site/', data={'username': 'username123', 'uname': 'username'})
		self.assertEqual(response.status_code, 200)
