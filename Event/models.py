from django import forms
from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.core.validators import *
from django.core.exceptions import ValidationError


class EventModel(models.Model):
    username = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    uname = models.CharField(max_length=200,  blank=True)
    class Meta:
	    verbose_name_plural = 'participant'
    def __str__(self):
        return self.uname
	

class Event(models.Model):
    eventsite = models.CharField(max_length=100, blank=True)
    eventname = models.CharField(max_length=100)
    eventdetail = models.CharField(max_length=1000)
    eventimage = models.CharField(max_length=300)
    eventdate =  models.CharField(max_length=100)
    eventlink = models.CharField(max_length=500, blank=True)
    participant = models.ManyToManyField(EventModel, blank=True)
    class Meta:
        verbose_name_plural = 'events'

    def __str__(self):
        return self.eventname