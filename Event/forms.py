from django.forms import ModelForm
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from .models import EventModel, Event
from django import forms

class EventForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(EventForm, self).__init__(*args, **kwargs)
		self.fields['username'].widget = forms.TextInput()
		self.fields['username'].label = ''
		self.fields['username'].required = 'True'
		self.fields['username'].widget.attrs = {'class': 'form-control', 'placeholder': 'Write your full name here...'}
		self.fields['username'].error_messages = {'max_length': 'Maksimal karakter adalah 100 karakter.'}

	class Meta:
		model = EventModel
		fields = ['username', 'created_date']
