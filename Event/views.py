from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import EventForm
from .models import EventModel, Event
from google.oauth2 import id_token
from google.auth.transport import requests


response = {}
# Create your views here.
def list(request):
	data = Event.objects.all()
	return render(request, 'Event/list.html', {'data':data})

def userevents(request):
	data = Event.objects.all()
	return render(request, 'Event/userevent.html', {'data':data})

def regist(request, site):
	event = get_object_or_404(Event, eventsite=site)
	if request.user.is_authenticated:
		data = EventModel.objects.all()
		form = EventForm(request.POST or None, request.FILES or None)
		if (request.method == "POST"):
			if form.is_valid():
				response['username'] = request.POST['username']
				response['uname'] = request.user.username
				schedule = EventModel(username = response['username'], uname = response['uname'])
				schedule.save()
				event.participant.add(schedule)
				event.save()
				return HttpResponseRedirect('/event/')
		else:
			form = EventForm()
			messages.error(request, "Error")
		return render(request, 'Event/regist.html', {'form':form, 'event':event})	
	else:
		return HttpResponseRedirect('/')

""" def confirm(request, event=None):
    events = Event.objects.all().filter(eventname=event)[0]
    user = request.user
    
    events.participant.add(user)    
    print(user.event_set.all())
    print(events.people.all())
    return HttpResponseRedirect('/yourlist/') """


