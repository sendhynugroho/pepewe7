from django.urls import path
from . import views
from .views import list
from .views import regist

#url for app
urlpatterns = [
    path('event/', views.list, name='list'),
    path('register/<str:site>/', views.regist, name='regist'),
    path('yourlist/', views.userevents, name='userevents'),
]
