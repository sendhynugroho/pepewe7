# Generated by Django 2.1.1 on 2018-10-19 14:51

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Event', '0005_auto_20181019_2137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventmodel',
            name='email',
            field=models.EmailField(max_length=50, validators=[django.core.validators.EmailValidator()]),
        ),
        migrations.AlterField(
            model_name='eventmodel',
            name='username',
            field=models.CharField(max_length=200),
        ),
    ]
