from django.contrib import admin
from .models import EventModel, Event

admin.site.register(Event)
admin.site.register(EventModel)
