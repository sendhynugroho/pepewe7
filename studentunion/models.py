from django.db import models
from django import forms
from django.core.validators import *
from django.core.exceptions import ValidationError

length_validation = MinLengthValidator(8)
class Registration(models.Model):
    name = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    email = models.EmailField(max_length=50, validators=[validate_email])
    birthplace = models.CharField(max_length=200)
    birthdate = models.DateField()
    password = models.CharField(max_length=20,)
    address = models.CharField(max_length=200)

class LoginPage(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    password = models.CharField(max_length=100)

    def as_dict(self) :
        return {
            "name" : self.name,
            "email" : self.email
        }

class Testimony(models.Model):
    name = models.CharField(max_length=200)
    testi = models.TextField(max_length=500)

    def as_dict(self) :
        return {
            "name" : self.name,
            "testi" : self.testi
        }

        