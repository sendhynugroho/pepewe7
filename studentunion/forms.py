from django import forms
from django.db import models
from .models import Registration, Testimony
from django.core.validators import *


length_validation = MinLengthValidator(8)

class TestiForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }
    
    name = forms.CharField(label = 'Your Name ?', max_length=200, widget=forms.TextInput(attrs=attrs), required = True)
    testi = forms.CharField(label = 'Write your own testimony ! ', max_length=500, widget=forms.Textarea(attrs=attrs), required = True)

class DateInput(forms.DateInput):
    input_type = 'date'


class PostForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    attrsDate = {
        'class' : 'form-control',
        'type' : 'date'
    }

    attrsPass = {
        'class' : 'form-control',
        'type' : 'password'
    }

    name = models.CharField(max_length=200)
    username = models.CharField(max_length=200)
    email = models.EmailField(max_length=50, blank=False, validators=[validate_email])
    birthplace = models.CharField(max_length=200)
    birthdate = models.DateField()
    password = models.CharField(max_length=20,)
    address = models.CharField(max_length=200)
        

    name = forms.CharField(label = 'Full Name :', max_length=200, widget=forms.TextInput(attrs=attrs), required = True )
    username = forms.CharField(label = 'Username :', max_length=200, widget=forms.TextInput(attrs=attrs), required = True )
    email = forms.EmailField(label = 'E-mail :', max_length=200, widget=forms.EmailInput(attrs=attrs), required = True )
    birthplace = forms.CharField(label = 'Place of Birth :', max_length=200, widget=forms.TextInput(attrs=attrs),required = True)
    birthdate = forms.DateField(label = 'Date of Birth :', widget=forms.DateInput(attrs=attrsDate))
    password = forms.CharField(label = 'Password :', min_length=8, max_length=20, widget=forms.PasswordInput(attrs=attrsPass), required =True)
    address = forms.CharField(label = 'Address :', max_length=200, widget=forms.TextInput(attrs=attrs), required =True)