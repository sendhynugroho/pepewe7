from django.shortcuts import render
from .forms import PostForm, TestiForm
from .models import Registration, Testimony
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse
from google.oauth2 import id_token
from google.auth.transport import requests
# from django.urls import reverse
# from django.shortcuts import get_object_or_404

response = {'author' : 'Steffi Alexandra'}

def landingpage(request):
    return render(request, 'studentunion/landingpage.html')

def studentregister(request):
    if request.method == "POST":
        response['name'] = request.POST['name']
        response['username'] = request.POST['username']
        response['email'] = request.POST['email']
        response['birthplace'] = request.POST['birthplace']
        response['birthdate'] = request.POST['birthdate']
        response['password'] = request.POST['password']
        response['address'] = request.POST['address']
        schedule = Registration(name = response['name'], username = response['username'], email = response['email'], birthdate=response['birthdate'], password=response['password'],  birthplace=response['birthplace'], address = response['address'])
        schedule.save()
        form = PostForm()
        jadwal = Registration.objects.all()
        html = 'studentunion/studentregister.html'
        context = {
            'form' : form,
            'jadwal' : jadwal
        }
        return render(request, html, context)
    else:
        form = PostForm()
        jadwal = Registration.objects.all()
        context = {
            'form' : form,
            'jadwal' : jadwal
        }
        return render(request, 'studentunion/studentregister.html', context)

def logout(request):
    response  = HttpResponseRedirect('/')
    response.delete_cookie('sessionid')
    return response

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            info = id_token.verify_oauth2_token(token, requests.Request(),
                                                  "366605632573-b6mond7b2883iekolh5n1d4anogs4jel.apps.googleusercontent.com")
            if info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            user_id = info['sub']
            email = info['email']
            name = info['name']
            request.session['user_id'] = user_id
            request.session['email'] = email
            request.session['name'] = name
            data = LoginPage(name = response['name'], username = response['username'], email = response['email'])
            data.save()
            return JsonResponse({"status": "0", "name": name})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'studentunion/login.html')

def getName(request):
    if request.user.is_authenticated:
        return JsonResponse({'name':request.user.username})
    else:
        return JsonResponse({'name' : 'null'})


def status(request):
    if request.method == "POST":
        response['name'] = request.POST['name']
        response['testi'] = request.POST['testi']
        test = Testimony(name = response['name'], testi = response['testi'])
        test.save()
        forms = TestiForm()
        stats = [obj.as_dict() for obj in Testimony.objects.all()]
        return JsonResponse({'objects' : stats})

def testipage(request):
    try:
        if request.user.is_authenticated:
            forms = TestiForm()
            tests = Testimony.objects.all()
            context = {
                'forms' : forms,
                'tests' : tests
            }
            return render(request, 'studentunion/about.html', context)
        else:
            return render(request, 'studentunion/about.html')
    except AttributeError:
        return render(request, 'studentunion/about.html')

