
function onSignIn(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
        sendToken(id_token);
    }

    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

    function sendToken(token) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/login",
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {id_token: token},
            success: function (result) {
                if (result.status === "0") {
                    $("#hi").text("Hello, " + result.name);
                    window.location.replace(result.url)
                } else {
                    alert("An error has occured!");
                }

            },
            error: function (error) {
                alert("An error has occured!");
            }
        })
    };


    $(document).ready(function () {
        gapi.load('auth2', function() {
            gapi.auth2.init();
        });
    });
