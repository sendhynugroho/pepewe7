from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpResponseRedirect
from django.http import HttpRequest
from google.oauth2 import id_token
from google.auth.transport import requests
from .views import landingpage, studentregister, logout, login, testipage, status
from .forms import PostForm

# Create your tests here.

class StudentUnionUnitTest(TestCase):
        def test_landingpage_url_exists(self):
                response = Client().get('/')
                self.assertEqual(response.status_code,200)

        def test_studentregister_url_exists(self):
                response = Client().get('/studentregister')
                self.assertEqual(response.status_code,301)

        def test_landing_text_exists(self):
                request = HttpRequest()
                response = landingpage(request)
                htmlResponse = response.content.decode('utf8')
                self.assertIn('Welcome', htmlResponse)

        def test_register_text_exists(self):
                request = HttpRequest()
                response = studentregister(request)
                htmlResponse = response.content.decode('utf8')
                self.assertIn('Care to Join Us?', htmlResponse)

        def test_form_todo_input_has_placeholder_and_css_classes(self):
                form = PostForm()
                self.assertIn('class="form-control"', form.as_p())

        def test_status(self):
                request = HttpRequest()
                request.method = "POST"
                request.POST['name'] = 'Steffi Alexandra'
                request.POST['username'] = 'steffiivy'
                request.POST['email'] = 'steffialexandraa@gmail.com'
                request.POST['birthplace'] = 'Jakarta'
                request.POST['birthdate'] = '1999-07-02'
                request.POST['password'] = 'abcde'
                request.POST['address'] = 'Kosan'
                studentregister(request)

        def test_using_func(self):
            found = resolve('/')
            self.assertEqual(found.func, landingpage)

        def test_login_url_is_exist(self):
            response = Client().get('/login/')
            self.assertEqual(response.status_code, 200)

        def test_login(self):
            found = resolve('/login/')
            self.assertEqual(found.func, login)

        def test_logout(self):
            found = resolve('/logout/')
            self.assertEqual(found.func, logout)

        def test_using_create_logout_func(self):
            request = HttpRequest()
            response  = HttpResponseRedirect('/')
            response.delete_cookie('sessionid')
            logout(request)

        def test_using_create_login_func(self):
            request = HttpRequest()
            request.method = "POST"
            request.POST['id_token'] = '1'
            login(request)

        def test_using_status(self):
            request = HttpRequest()
            request.method = "POST"
            request.POST['name'] = 'rani'
            request.POST['testi'] = 'test123'
            status(request)

        def test_about_url_exists(self):
                response = Client().get('/about')
                self.assertEqual(response.status_code,301)

        def test_about_text_exists(self):
                request = HttpRequest()
                response = testipage(request)
                htmlResponse = response.content.decode('utf8')
                self.assertIn('a b o u t   u s.', htmlResponse)

        def test_bgimage_exists(self):
                request = HttpRequest()
                response = testipage(request)
                htmlResponse = response.content.decode('utf8')
                self.assertIn('bgimg', htmlResponse)
