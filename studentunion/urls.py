from django.urls import path
from . import views
from django.conf.urls import include
from .views import landingpage, studentregister, login, logout, status, getName, testipage

#url for app
urlpatterns = [
    path('', views.landingpage, name = 'landingpage'),
    path('about/', views.testipage, name = 'about'),
    path('status/', views.status, name = 'status'),
    path('studentregister/', views.studentregister, name = 'studentregister'),
    path('login/', views.login, name='login'),
    path('getName/', views.getName, name='getName'),
    path('logout/', views.logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]
