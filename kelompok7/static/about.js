$(document).ready(function(){
    findName()
    $('.form-control').keyup(function(){
        var name = document.getElementById("id_name");
        var testi = document.getElementById("id_testi");
        if(name.value.length != 0 && testi.value.length != 0){
            $('#register').prop('disabled',false);
        }
        else{
            $('#register').prop('disabled',true);
        }
    });

    $('#register').click(function(event){
        event.preventDefault();
        console.log("masuk ga si")
        var name = document.getElementById("id_name").value;
        var testi = document.getElementById("id_testi").value;
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            data: {name: name, testi:testi},
            url: "/status/",
            headers: {
                "X-CSRFToken": csrftoken
            },
            success: function (hasil) {
                console.log(hasil)
                alert("Thank you for sending your testimony!");
                var data = hasil.objects
                $('table td').remove()
                for (var x = 0; x < data.length; x++) {
                    tr = $('<tr/>');
                    tr.append("<td>" + data[x].name + "</td>");
                    tr.append("<td>" + data[x].testi + "</td>");
                    $('table').append(tr);
                }
            },
            error: function (hasil) {
                alert("An error has occured!");
            }
        });
    });


    function findName(){
        $.ajax({
            method: "GET",
            url: "/getName", // url yang akan dipanggil
            success: function (hasil) {
                $('#register').show();
                name = hasil.name;
                
            }
        });
    }

});


