from django.urls import path
from . import views
from .views import news

urlpatterns = [
    path('news/', views.news, name='news'),
]
